const add = document.querySelector('#add-book button'); //lembrar que é cosnt e não let

add.addEventListener('click', (e) => {

    e.preventDefault();

    let books = document.querySelector('#book-list ul');

    console.log(books);
    
    let newBook = document.querySelector('#add-book input').value;

    let newLi = document.createElement("li");

    let newSpan = document.createElement("span");
    newSpan.classList.add("name");
    newSpan.innerHTML = newBook;
    newLi.appendChild(newSpan);

    newSpan = document.createElement("span");
    newSpan.classList.add("delete");
    newSpan.innerHTML = "excluir";
    newLi.appendChild(newSpan);

    console.log(newLi);

    newBook.appendChild(newLi);
})

const items = document.querySelector('#book-list');

items.addEventListener('click', (event) => {

    if (event.target.className === 'delete'){

        let book = event.target.parentNode
        book.parentNode.removeChild(book);
        console.log('Item excluído');
    }
})